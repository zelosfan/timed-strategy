package de.zelosfan.timedstrategy.gameStates;

import com.badlogic.gdx.InputMultiplexer;
import de.zelosfan.framework.GameState.GameState;
import de.zelosfan.framework.GameState.GameStateManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.framework.Timing.GametimeManager;
import de.zelosfan.timedstrategy.Game;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.entity.Entity;
import de.zelosfan.timedstrategy.world.Building;
import de.zelosfan.timedstrategy.world.Tile;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 03:57
 */
public class Tutorial extends GameState{

    public Tutorial(GameStateManager gameStateManager, boolean _inputPreProcessor) {
        super(gameStateManager, _inputPreProcessor);
    }

    @Override
    public void render(Rendermanager rendermanager) {
        super.render(rendermanager);
        gameStateManager.instanceHashMap.get("Tutorial").render(rendermanager);
    }

    @Override
    protected InputMultiplexer buildInputMultiplexer() {
        return new InputMultiplexer(gameStateManager.instanceHashMap.get("Tutorial"));
    }

    @Override
    public void onActivate() {
    }

    @Override
    public void onDeactivate() {
    }

    @Override
    public void tick() {
    }
}
