package de.zelosfan.timedstrategy.gameStates;

import com.badlogic.gdx.InputMultiplexer;
import de.zelosfan.framework.GameState.GameState;
import de.zelosfan.framework.GameState.GameStateManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.framework.Timing.GametimeManager;
import de.zelosfan.timedstrategy.Main;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 03:57
 */
public class IngamePlanning extends GameState{
    public int tickC;

    public IngamePlanning(GameStateManager gameStateManager, boolean _inputPreProcessor) {
        super(gameStateManager, _inputPreProcessor);
        tickC = GametimeManager.realtimeToGametime(10);
    }

    @Override
    public void render(Rendermanager rendermanager) {
        super.render(rendermanager);

        gameStateManager.instanceHashMap.get("MapDisplay").render(rendermanager);
        gameStateManager.instanceHashMap.get("UserInterface").render(rendermanager);
    }

    @Override
    protected InputMultiplexer buildInputMultiplexer() {
        return new InputMultiplexer(gameStateManager.instanceHashMap.get("UserInterface"), gameStateManager.instanceHashMap.get("MapDisplay"));
    }

    @Override
    public void onActivate() {
        gameStateManager.instanceHashMap.get("UserInterface").onActivate();
        gameStateManager.instanceHashMap.get("MapDisplay").onActivate();
    }

    @Override
    public void onDeactivate() {
        gameStateManager.instanceHashMap.get("UserInterface").onDeactivate();
        gameStateManager.instanceHashMap.get("MapDisplay").onDeactivate();
    }

    @Override
    public void tick() {
        tickC--;
        Main.game.tick();
        gameStateManager.instanceHashMap.get("MapDisplay").tick();
        gameStateManager.instanceHashMap.get("UserInterface").tick();
        Main.game.timeLeft = tickC;
        if (tickC <= 0) {
            Main.game.changeToAttackingPhase();
        }
    }
}
