package de.zelosfan.timedstrategy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import de.zelosfan.framework.Algorithms.Pathable;
import de.zelosfan.framework.Effect.Effect;
import de.zelosfan.timedstrategy.ai.Ai;
import de.zelosfan.timedstrategy.algorithms.AStarCus;
import de.zelosfan.timedstrategy.entity.Entity;
import de.zelosfan.timedstrategy.etc.AttackedEffect;
import de.zelosfan.timedstrategy.etc.CurrentAction;
import de.zelosfan.timedstrategy.etc.Reste;
import de.zelosfan.timedstrategy.gameStates.IngameAttack;
import de.zelosfan.timedstrategy.gameStates.IngamePlanning;
import de.zelosfan.timedstrategy.gameStates.Tutorial;
import de.zelosfan.timedstrategy.gameStates.instances.MapDisplay;
import de.zelosfan.timedstrategy.gameStates.instances.UserInterface;
import de.zelosfan.timedstrategy.team.Team;
import de.zelosfan.timedstrategy.world.Building;
import de.zelosfan.timedstrategy.world.Tile;
import de.zelosfan.timedstrategy.world.World;
import de.zelosfan.timedstrategy.world.buildingtypes.BuildingFactory;
import de.zelosfan.timedstrategy.world.buildingtypes.BuildingTyp;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 04:05
 */
public class Game {

    public World world;
    public static Team redTeam = new Team(Color.RED, true);
    public static Team blueTeam = new Team(Color.BLUE, false);

    public int timeLeft = 600;

    public ArrayList<Pathable> visible = new ArrayList<>();
    public CurrentAction currentAction = CurrentAction.NOTHING;
    public ArrayList<Pathable> avaible = new ArrayList<>();
    public LinkedList<Pathable> highlighted = new LinkedList<>();
    public Object current0 = null;

    public String showMessage = "";
    public int toScreen = -1;

    public LinkedList<Effect> effects = new LinkedList<>();

    public Game(int mapID) {
        redTeam = new Team(Color.RED, true);
        blueTeam = new Team(Color.BLUE, false);

        world = new World(Main.mapObjectMap.get("map" + mapID));
        Main.gameStateManager.instanceHashMap.put("MapDisplay", new MapDisplay());
        Main.gameStateManager.instanceHashMap.put("UserInterface", new UserInterface());

        Main.gameStateManager.setCurrentGameState(new IngamePlanning(Main.gameStateManager, true));
    }

    public void win() {
//        Main.audioManager.playSound("fail.wav");
        showMessage = "You win! :D";

        Main.map++;
        toScreen = 180;
    }

    public void lose() {
        Main.audioManager.playSound("fail.wav");
        showMessage = "You lose :( Try again!";

        toScreen = 180;
    }

    public void tick() {
        if (toScreen > 0)  {
         toScreen--;
        } else if (toScreen == 0) {
            Main.gameStateManager.setCurrentGameState(new Tutorial(Main.gameStateManager, true));
        }

        for (Effect effect: effects) {
            effect.tick();
        }
    }

    public void checkEntities() {
        LinkedList<Entity> dumpList = new LinkedList<>();

        for (Entity entity: world.entities) {
            if (entity.health <= 0) {
                Main.audioManager.playSound("explosion0.wav");
                effects.add(new Reste(entity));
                entity.tile.entity = null;
                entity.tile.locked = false;
                entity.tile = null;
                dumpList.add(entity);
            }
        }

        for (Entity entity: dumpList) {
            world.entities.remove(entity);
        }

    }

    public void updateFogOfWar() {
        visible.clear();
        for (Entity entity: Main.game.world.entities) {
            if (entity.team.equals(blueTeam)) {
                AStarCus.fogOfWar(world.tiles, visible, entity.tile, entity.entitytyp.sight);
            }
        }
    }

    public void activateSpace() {
        if (!(Main.gameStateManager.getCurrentGameState() instanceof IngamePlanning)) return;

        if (blueTeam.moneydeduct(1)) {
            ((IngamePlanning) Main.gameStateManager.getCurrentGameState()).tickC += 600;
            Main.audioManager.playSound("spawn0.wav");
        }
    }

    public void onClickTile(int x, int y) {
        if (x < 0 || y < 0 || x > world.WORLD_WIDTH - 1 || y > world.WORLD_HEIGHT - 1) return;

        Tile tile = world.tiles[x][y];

        if (tile instanceof Building && tile.entity == null) {
            ((BuildingTyp) tile.tiletyp).onClick((Building) tile);
        }

        switch (currentAction) {
            case NOTHING:
                if (tile.entity != null) {
                    currentAction = CurrentAction.SELECTED_ENTITY;
                    current0 = tile.entity;

                    avaible = AStarCus.expandPath(world.tiles, tile, tile.entity.entitytyp.speed);
                }
                break;

            case SELECTED_ENTITY:
                if (highlighted.contains(tile)) {
                    ((Entity) current0).waypoints = highlighted;
                }
                currentAction = CurrentAction.NOTHING;
                break;

            case TO_SELECT_ENTITY:
                if (tile.entity != null) {

                    if (blueTeam.moneydeduct(tile.entity.entitytyp.cost)) {
                        ((Building) current0).obj = tile.entity.entitytyp;
                        ((Building) current0).locked = true;
                    } else {
                        Main.audioManager.playSound("fail.wav");
                    }

                    currentAction = CurrentAction.NOTHING;
                    Main.game.showMessage = "";
                }

                break;

        }
    }

    public void onHoveTile(int x, int y) {
        if (x < 0 || y < 0 || x > world.WORLD_WIDTH - 1 || y > world.WORLD_HEIGHT - 1) return;

        Tile tile = world.tiles[x][y];

        switch (currentAction) {
            case SELECTED_ENTITY:
                if (avaible.contains(tile)) {
                    highlighted = AStarCus.getAStarPath(world.tiles, ((Entity) current0).tile, tile, 1, false);

//                    avaible = AStarCus.expandPath(world.tiles, tile, tile.entity.entitytyp.speed);
                }
                break;

        }
    }

    public void changeToPlanningPhase() {
        Main.gameStateManager.setCurrentGameState(new IngamePlanning(Main.gameStateManager, true));
        currentAction = CurrentAction.NOTHING;

        visible.clear();
        for (Entity entity: Main.game.world.entities) {
            entity.offsetY = 0;
            entity.offsetX = 0;

            if (entity.team.equals(blueTeam)) {
                AStarCus.fogOfWar(world.tiles, visible, entity.tile, entity.entitytyp.sight);
            }
        }

        for (int i = 0; i < world.tiles.length; i++) {
            for (int l = 0; l < world.tiles[i].length; l++) {
                if (world.tiles[i][l].entity == null) {
                    world.tiles[i][l].locked = false;
                }

                if (world.tiles[i][l] instanceof Building) {
                    Building building = (Building) world.tiles[i][l];
                    if (world.tiles[i][l].entity != null) {
                        if (((Building) world.tiles[i][l]).team == null) {
                            ((BuildingTyp) world.tiles[i][l].tiletyp).onCapture((Building) world.tiles[i][l], world.tiles[i][l].entity);
                        } else if (!world.tiles[i][l].entity.team.equals(((Building) world.tiles[i][l]).team)) {
                            ((BuildingTyp) world.tiles[i][l].tiletyp).onCapture((Building) world.tiles[i][l], world.tiles[i][l].entity);
                        }
                    }

                    ((BuildingTyp) ((Building) world.tiles[i][l]).tiletyp).nextRound((Building) world.tiles[i][l]);

                    if (building.team != null) {
                        if (building.team.equals(Game.redTeam)) {
                            if (building.tiletyp instanceof BuildingFactory) {
                                Ai.buildUnit(building);
                            }
                        }
                    }
                }
            }
        }

    }


    public void changeToAttackingPhase() {
        Main.gameStateManager.setCurrentGameState(new IngameAttack(Main.gameStateManager, true));
        currentAction = CurrentAction.ATTACK;
        Main.game.showMessage = "";

        for (Entity entity: world.entities) {
            if (entity.team.equals(Game.redTeam)) {
                Ai.checkRadius(entity);
                if (entity.target == null) {
                    entity.waypoints = Ai.getAiPath(world.tiles, entity, entity.tile, Game.blueTeam.hq);
                } else {
                    entity.waypoints = Ai.getAiPath(world.tiles, entity, entity.tile, entity.target);
                }
            }
        }
    }

    public int calculateFightCost(Entity main, Tile newTile, Entity entity1) { //Lower the better (-100 to 100)
        int f1 = (int) (((int) ( (main.health / 120f + 0.2f) * main.entitytyp.attackStats.get(entity1.entitytyp))) * Repository.coverBonus.get(entity1.tile.tiletyp.cover));
        int f2 = (int) (((int) ( (entity1.health / 120f + 0.2f) * entity1.entitytyp.attackStats.get(main.entitytyp))) * Repository.coverBonus.get(newTile.tiletyp.cover));

        f1 = entity1.entitytyp.cost * f1;
        f2 = main.entitytyp.cost * f2;
        return f2 - f1;
    }

    public void fightEntities(Entity entity0, Entity entity1) {
        if (entity0 == null || entity1 == null) return;
        if (entity0.health < 0 || entity1.health < 0) return;
        if (entity0.tile == null || entity1.tile == null) return;


        int h2 = entity1.health;

        entity1.damage((int) ( (entity0.health / 120f + 0.2f) * entity0.entitytyp.attackStats.get(entity1.entitytyp)));
        entity0.damage((int) ( (h2 / 120f + 0.2f) * entity1.entitytyp.attackStats.get(entity0.entitytyp)));

        Main.audioManager.playSound("hit0.wav");

        effects.add(new AttackedEffect(entity0));
        effects.add(new AttackedEffect(entity1));

//        Gdx.app.debug("", "" + entity0.health + "#" + entity1.health);

     /*   h2 = entity1.health;

        entity1.damage((int) ( (entity0.health / 100f) * entity0.entitytyp.attackStats.get(entity1.entitytyp)));
        entity0.damage((int) ( (h2 / 100f) * entity1.entitytyp.attackStats.get(entity0.entitytyp)));

        Gdx.app.debug("", "" + entity0.health + "#" + entity1.health); */


//        entity0.waypoints.clear();
//        entity1.waypoints.clear();
    }

    public void attackAround(Entity entity) {
        for (int i = -1; i <= 1; i++) {
            for (int l = -1; l <= 1; l++) {
                if (Math.abs(i) != Math.abs(l)) {
                    if (entity.tile.posX + i > 0 && entity.tile.posX + i < world.WORLD_WIDTH && entity.tile.posY + l > 0 && entity.tile.posY + l < world.WORLD_HEIGHT) {
                        if (world.tiles[entity.tile.posX + i][entity.tile.posY + l].entity != null) {
                            if (!world.tiles[entity.tile.posX + i][entity.tile.posY + l].entity.team.equals(entity.team) && world.tiles[entity.tile.posX + i][entity.tile.posY + l].entity.health > 0) {
                                fightEntities(entity, world.tiles[entity.tile.posX + i][entity.tile.posY + l].entity);
                            }
                        }
                    }
                }
            }
        }
    }

}
