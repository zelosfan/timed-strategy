package de.zelosfan.timedstrategy.etc;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 09:36
 */
public class Etc {
    public static double round(double unrounded, int precision, RoundingMode roundingMode) {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.doubleValue();
    }
}
