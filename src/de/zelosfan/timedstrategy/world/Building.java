package de.zelosfan.timedstrategy.world;

import de.zelosfan.timedstrategy.team.Team;
import de.zelosfan.timedstrategy.world.buildingtypes.BuildingTyp;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 04:17
 */
public class Building extends Tile{

    public Team team;
    public int counter = 0;
    public Object obj = null;

    public Building(int _posX, int _posY, BuildingTyp _tiletyp, Team _team) {
        super(_posX, _posY, _tiletyp);
        team = _team;
    }
}
