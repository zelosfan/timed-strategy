package de.zelosfan.timedstrategy.world.buildingtypes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.entity.Entity;
import de.zelosfan.timedstrategy.team.Team;
import de.zelosfan.timedstrategy.world.Building;
import de.zelosfan.timedstrategy.world.Tiletyp;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 21:25
 */
public abstract class BuildingTyp extends Tiletyp {

    public final TextureRegion textureRegion2;
    public final int wealth;

    public BuildingTyp(String _texture, int _waycost, int _cover, int _wealth) {
        super(_texture, _waycost, _cover);
        wealth = _wealth;
        textureRegion2 = Main.textureRegionObjectMap.get(texture + "1");
    }

    public void onClick(Building building) {

    }

    public void nextRound(Building building) {

    }

    public void onCapture(Building building, Entity entity) {
        building.team = entity.team;
    }

    public void render(Rendermanager rendermanager, Building building) {

    }

}
