package de.zelosfan.timedstrategy.world.buildingtypes;

import com.badlogic.gdx.graphics.Color;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.timedstrategy.Game;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.etc.RisingCoin;
import de.zelosfan.timedstrategy.gameStates.instances.MapDisplay;
import de.zelosfan.timedstrategy.world.Building;
import de.zelosfan.timedstrategy.world.Tile;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 21:31
 */
public class BuildingIncome extends BuildingTyp{

    public final int time;
    public final int money;

    public BuildingIncome(String texture, int waycost, int cover, int _time, int _money) {
        super(texture, waycost, cover, 2);
        time = _time;
        money = _money;
    }

    @Override
    public void nextRound(Building building) {
        if (building.team != null) {
            building.counter++;
            if (building.counter > time) {
                building.team.moneyadd(money);

                if (building.team.equals(Game.blueTeam)) {
                    Main.game.effects.add(new RisingCoin(building));
                }
                building.counter = 0;
            }
        }
    }


    public void render(Rendermanager rendermanager, Building building) {
        if (building.team == null) return;
        if (building.team.equals(Game.redTeam)) return;

        rendermanager.spriteBatch.draw(Main.textureRegionObjectMap.get("clock"), Tile.SIZE * building.posX + Tile.SIZE * 0.5f, Tile.SIZE * building.posY + Tile.SIZE * 0.5f, Tile.SIZE * 0.6f, Tile.SIZE * 0.6f);
        MapDisplay.drawIndependentText(rendermanager, Main.fontMap.get("tektonpro").get(24), "font", "" + (time - building.counter), Tile.SIZE * building.posX + Tile.SIZE * 0.66f, Tile.SIZE * building.posY + Tile.SIZE * 1.1f, Color.BLACK);
        rendermanager.spriteBatch.setColor(Color.WHITE);
    }
}
