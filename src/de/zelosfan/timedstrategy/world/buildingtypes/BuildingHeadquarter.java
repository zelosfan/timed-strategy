package de.zelosfan.timedstrategy.world.buildingtypes;

import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.entity.Entity;
import de.zelosfan.timedstrategy.world.Building;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.08.13
 * Time: 09:52
 */
public class BuildingHeadquarter extends BuildingTyp{
    public BuildingHeadquarter(String _texture, int _waycost, int _cover) {
        super(_texture, _waycost, _cover, 5);
    }

    @Override
    public void onCapture(Building building, Entity entity) {
        if (building.team.equals(Main.game.blueTeam)) {
            Main.game.lose();
        } else {
            Main.game.win();
        }

        building.team = entity.team;
    }
}
