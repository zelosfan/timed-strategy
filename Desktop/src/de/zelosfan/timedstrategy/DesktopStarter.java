package de.zelosfan.timedstrategy;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 03:35
 */
public class DesktopStarter {
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "TimedStrategy";
        cfg.useGL20 = true;
        cfg.width = 1024;
        cfg.height = 768;
        cfg.resizable = false;
        new LwjglApplication(new Main(), cfg);
    }
}
