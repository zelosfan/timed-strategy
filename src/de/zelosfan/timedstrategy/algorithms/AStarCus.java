package de.zelosfan.timedstrategy.algorithms;

import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.framework.Algorithms.Pathable;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 06:18
 */
public class AStarCus {
    public static ArrayList<Pathable> expandPath(Pathable[][] nodes, Pathable start, int limit) {
        ArrayList<Pathable> closedList = new ArrayList<>();
        ArrayList<Pathable> accessable = new ArrayList<>();
        LinkedList<Pathable> openList = new LinkedList<>();

        ObjectMap<Pathable, Integer> g_score = new ObjectMap<>(nodes.length + nodes[0].length);
        ObjectMap<Pathable, Integer> f_score = new ObjectMap<>(nodes.length + nodes[0].length);
        g_score.put(start, 0);
        f_score.put(start, start.getWaycost());


        openList.add(start);

        while (openList.size() != 0) {

            Pathable current = openList.getFirst();
            int lowestscore = f_score.get(openList.getFirst());
            for (Pathable node : openList) {
                if (f_score.get(node) < lowestscore) {
                    current = node;
                    lowestscore = f_score.get(node);
                }
            }

            openList.remove(current);
            closedList.add(current);

            for (int i = -1; i <= 1; i++) {
                for (int l = -1; l <= 1; l++) {
                    if (current.getPositionV2().x + i < nodes.length && current.getPositionV2().y + l < nodes[0].length && current.getPositionV2().x + i >= 0 && current.getPositionV2().y + l >= 0) {
                        if (current.isWalkable(current.equals(start))) {
                            if (Math.abs(i) != Math.abs(l)) {
                                Pathable neighbor = nodes[((int) current.getPositionV2().x + i)][((int) current.getPositionV2().y + l)];
                                int additionalResistance = neighbor.getWaycost();
                                int tentative_g_score = g_score.get(current) + additionalResistance;
                                if (closedList.contains(neighbor) && tentative_g_score >= g_score.get(neighbor)) {
                                    continue;
                                }

                                if (!openList.contains(neighbor) || tentative_g_score < g_score.get(neighbor)) {
                                    g_score.put(neighbor, tentative_g_score);
                                    f_score.put(neighbor, g_score.get(neighbor));

                                    if (!openList.contains(neighbor) && tentative_g_score <= limit) {
                                        openList.add(neighbor);
                                        if (!accessable.contains(neighbor)) {
                                            accessable.add(neighbor);
                                        }
                                    }

                                }

                            }
                        } else {
                            closedList.add(current);
                        }
                    }
                }
            }

        }
        return accessable;
    }

    public static ArrayList<Pathable> fogOfWar(Pathable[][] nodes, ArrayList<Pathable> accessable, Pathable start, int limit) {
        ArrayList<Pathable> closedList = new ArrayList<>();
        LinkedList<Pathable> openList = new LinkedList<>();

        ObjectMap<Pathable, Integer> g_score = new ObjectMap<>(nodes.length + nodes[0].length);
        ObjectMap<Pathable, Integer> f_score = new ObjectMap<>(nodes.length + nodes[0].length);
        g_score.put(start, 0);
        f_score.put(start, 1);


        openList.add(start);
        accessable.add(start);

        while (openList.size() != 0) {

            Pathable current = openList.getFirst();
            int lowestscore = f_score.get(openList.getFirst());
            for (Pathable node : openList) {
                if (f_score.get(node) < lowestscore) {
                    current = node;
                    lowestscore = f_score.get(node);
                }
            }

            openList.remove(current);
            closedList.add(current);

            for (int i = -1; i <= 1; i++) {
                for (int l = -1; l <= 1; l++) {
                    if (current.getPositionV2().x + i < nodes.length && current.getPositionV2().y + l < nodes[0].length && current.getPositionV2().x + i >= 0 && current.getPositionV2().y + l >= 0) {
                        if (current.isWalkable(current.equals(start))) {
                            if (Math.abs(i) != Math.abs(l)) {
                                Pathable neighbor = nodes[((int) current.getPositionV2().x + i)][((int) current.getPositionV2().y + l)];
                                int additionalResistance = 1;
                                int tentative_g_score = g_score.get(current) + additionalResistance;
                                if (closedList.contains(neighbor) && tentative_g_score >= g_score.get(neighbor)) {
                                    continue;
                                }

                                if (!openList.contains(neighbor) || tentative_g_score < g_score.get(neighbor)) {
                                    g_score.put(neighbor, tentative_g_score);
                                    f_score.put(neighbor, g_score.get(neighbor));

                                    if (!openList.contains(neighbor) && tentative_g_score <= limit) {
                                        openList.add(neighbor);
                                        if (!accessable.contains(neighbor)) {
                                            accessable.add(neighbor);
                                        }
                                    }

                                }

                            }
                        } else {
                            closedList.add(current);
                        }
                    }
                }
            }

        }
        return accessable;
    }




    public static LinkedList<Pathable> getAStarPath(Pathable[][] nodes, Pathable start, Pathable goal, int standardWaycost, boolean allowVertical) {
        ArrayList<Pathable> closedList = new ArrayList<>();
        LinkedList<Pathable> openList = new LinkedList<>();

        ObjectMap<Pathable, Pathable> came_from = new ObjectMap<>();

        ObjectMap<Pathable, Integer> g_score = new ObjectMap<>(nodes.length + nodes[0].length);
        ObjectMap<Pathable, Integer> f_score = new ObjectMap<>(nodes.length + nodes[0].length);

        g_score.put(start, 0);

        if (allowVertical) {
            f_score.put(start, (int) (g_score.get(start) + Math.abs(start.getPositionV2().dst(goal.getPositionV2()))) * standardWaycost);
        } else {
            f_score.put(start, (int) (g_score.get(start) + Math.abs(start.getPositionV2().x - goal.getPositionV2().x) + Math.abs(start.getPositionV2().y - goal.getPositionV2().y)) * standardWaycost);
        }


        openList.add(start);

        while (openList.size() != 0) {

            Pathable current = openList.getFirst();
            int lowestscore = f_score.get(openList.getFirst());
            for (Pathable node : openList) {
                if (f_score.get(node) < lowestscore) {
                    current = node;
                    lowestscore = f_score.get(node);
                }
            }


            if (current.equals(goal)) {
                return reconstructPath(came_from, goal);
            }

            openList.remove(current);
            closedList.add(current);

            for (int i = -1; i <= 1; i++) {
                for (int l = -1; l <= 1; l++) {
                    if (current.getPositionV2().x + i < nodes.length && current.getPositionV2().y + l < nodes[0].length && current.getPositionV2().x + i >= 0 && current.getPositionV2().y + l >= 0) {
                        if (current.isWalkable(current.equals(start))) {
                            if (Math.abs(i) != Math.abs(l) || allowVertical) {
                                Pathable neighbor = nodes[((int) current.getPositionV2().x + i)][((int) current.getPositionV2().y + l)];
                                //    int additionalResistance = Main.world.getTile((int) neighbor.x,(int) neighbor.y).getAdditionWaycost();
                                int additionalResistance = neighbor.getWaycost();

                                int heuristic;
                                if (allowVertical) {
                                    heuristic = (int) ( Math.abs(neighbor.getPositionV2().dst(goal.getPositionV2()) * standardWaycost));
                                } else {
                                    heuristic = (int) (( Math.abs(neighbor.getPositionV2().x - goal.getPositionV2().x) + Math.abs(neighbor.getPositionV2().y - goal.getPositionV2().y)) * standardWaycost);
                                }

                                int tentative_g_score = g_score.get(current) + additionalResistance;


                                if (closedList.contains(neighbor) && tentative_g_score >= g_score.get(neighbor)) {
                                    continue;
                                }

                                if (!openList.contains(neighbor) || tentative_g_score < g_score.get(neighbor)) {
                                    came_from.put(neighbor, current);
                                    g_score.put(neighbor, tentative_g_score);
                                    f_score.put(neighbor, g_score.get(neighbor) + heuristic);

                                    if (!openList.contains(neighbor)) {
                                        openList.add(neighbor);
                                    }

                                }

                            }
                        } else {
                            closedList.add(current);
                        }
                    }
                }
            }

        }
        return null;

    }

    public static LinkedList<Pathable> reconstructPath(ObjectMap<Pathable, Pathable> came_from, Pathable current) {
        if (came_from.get(current) != null) {
            LinkedList<Pathable> p = reconstructPath(came_from, came_from.get(current));
            p.add(current);
//            Main.world.getTile((int) current.x, (int) current.y).tiletyp = Main.tiletypHashMap.get("test2");
            return p;
        } else {
            LinkedList<Pathable> p = new LinkedList<>();
            p.add(current);
            return p;
        }
    }
}
