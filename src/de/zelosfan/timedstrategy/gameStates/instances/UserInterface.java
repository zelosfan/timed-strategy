package de.zelosfan.timedstrategy.gameStates.instances;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector3;
import de.zelosfan.framework.Effect.Effect;
import de.zelosfan.framework.GameState.Instance;
import de.zelosfan.framework.Intelligence.PlatformInformation;
import de.zelosfan.framework.Logging.LogManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.framework.Timing.GametimeManager;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.etc.CurrentAction;
import de.zelosfan.timedstrategy.etc.Etc;
import de.zelosfan.timedstrategy.gameStates.IngameAttack;
import de.zelosfan.timedstrategy.gameStates.IngamePlanning;
import de.zelosfan.timedstrategy.world.Tile;

import java.math.RoundingMode;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 04:01
 */
public class UserInterface implements Instance{
    @Override
    public void onActivate() {

    }

    @Override
    public void onDeactivate() {

    }

    public void drawIndependentText(Rendermanager rendermanager, BitmapFont font, String shader, String text, float x, float y, Color color) {
        rendermanager.spriteBatch.setShader(Main.shaderProgramObjectMap.get(shader));
        font.setColor(color);
        font.draw(rendermanager.spriteBatch, text,  Gdx.graphics.getWidth() * x + rendermanager.camera.position.x - rendermanager.camera.viewportWidth / 2, Gdx.graphics.getHeight() * y + rendermanager.camera.position.y - rendermanager.camera.viewportHeight / 2);
        rendermanager.clearShader();
        rendermanager.spriteBatch.setShader(null);
    }

    @Override
    public void render(Rendermanager rendermanager) {
        for (Effect effect: Main.game.effects) {
            effect.render(rendermanager);
        }


        rendermanager.setShader("fontOutline");
        Main.fontMap.get("tektonpro").get(28).setColor(Color.WHITE);

        double ts = GametimeManager.gametimeToRealtime(Main.game.timeLeft);
        ts = Etc.round(ts, 2, RoundingMode.HALF_UP);

        if (Main.gameStateManager.getCurrentGameState() instanceof IngameAttack) {
            rendermanager.drawIndependent(Main.textureRegionObjectMap.get("timera"), 0.35f, 0.8f, 0.3f, 0.4f, 0);
        } else {
            rendermanager.drawIndependent(Main.textureRegionObjectMap.get("timer"), 0.35f, 0.8f, 0.3f, 0.4f, 0);
        }


        rendermanager.drawIndependent(Main.textureRegionObjectMap.get("clock"), 0.43f, 0.87f, 0.05f, 1f, 0);
        Color color = Color.GREEN;

        if (ts < 5) {
            color = Color.YELLOW;
        }
        if (ts < 2) {
            color = Color.RED;
        }

        drawIndependentText(rendermanager, Main.fontMap.get("tektonpro").get(32), "font", "" + ts, 0.48f, 0.93f, color);

        rendermanager.drawIndependent(Main.textureRegionObjectMap.get("moneyDisplay"), 0.85f, 0.9f, 0.1f, 0.4f, 0);
        drawIndependentText(rendermanager, Main.fontMap.get("tektonpro").get(32), "font", "" + Main.game.blueTeam.money, 0.895f, 0.95f, Color.BLACK);
        rendermanager.drawIndependent(Main.textureRegionObjectMap.get("10s"), 0.858f, 0.898f, 0.04f, 1f, 0);

        if (!Main.game.showMessage.equals("")) {
            rendermanager.drawIndependent(Main.textureRegionObjectMap.get("showMessage"), 0.2f, 0.05f, 0.6f, 0.14f, 0);
            float size = Main.fontMap.get("tektonpro").get(32).getBounds(Main.game.showMessage).width / 2f / Gdx.graphics.getWidth();
            drawIndependentText(rendermanager, Main.fontMap.get("tektonpro").get(32), "font", Main.game.showMessage, 0.5f - size, 0.135f, Color.BLACK);
        }

    }

    @Override
    public void tick() {

    }

    @Override
    public void connected(Controller controller) {

    }

    @Override
    public void disconnected(Controller controller) {

    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        return false;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.CONTROL_LEFT:
                if (Main.gameStateManager.getCurrentGameState() instanceof IngamePlanning) {
                    ((IngamePlanning) Main.gameStateManager.getCurrentGameState()).tickC = 1;
                }
                break;
            case Input.Keys.SPACE:
                Main.game.activateSpace();
                break;

            case Input.Keys.ESCAPE:
                if (Main.game.currentAction != CurrentAction.ATTACK) {
                    Main.game.currentAction = CurrentAction.NOTHING;
                    Main.game.showMessage = "";
                }
                break;

            case Input.Keys.F1:
                Main.SHOW_HELP = !Main.SHOW_HELP;
                break;

            case Input.Keys.F11:
                PlatformInformation.setFullscreen(!Main.FULLSCREEN);
                Main.FULLSCREEN = !Main.FULLSCREEN;
                break;

            case Input.Keys.F8:
                Main.game.toScreen = 1;
                Main.map++;
                break;

        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
//        LogManager.log(UserInterface.class, Application.LOG_DEBUG, screenX + " # " +screenY);

        if (button == Input.Buttons.LEFT) {
        int tileX = (int) (Main.rendermanager.camera.position.x - Main.rendermanager.camera.viewportWidth / 2 + screenX);
        tileX /= Tile.SIZE;
        int tileY = (int) (Main.rendermanager.camera.position.y - Main.rendermanager.camera.viewportHeight / 2 + screenY);
        tileY /= Tile.SIZE;

        Main.game.onClickTile(tileX, tileY);
        }

        if (button == Input.Buttons.RIGHT) {
            if (Main.game.currentAction != CurrentAction.ATTACK) {
                Main.game.currentAction = CurrentAction.NOTHING;
                Main.game.showMessage = "";
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        int tileX = (int) (Main.rendermanager.camera.position.x - Main.rendermanager.camera.viewportWidth / 2 + screenX);
        tileX /= Tile.SIZE;
        int tileY = (int) (Main.rendermanager.camera.position.y - Main.rendermanager.camera.viewportHeight / 2 + screenY);
        tileY /= Tile.SIZE;

        Main.game.onHoveTile(tileX, tileY);

        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
