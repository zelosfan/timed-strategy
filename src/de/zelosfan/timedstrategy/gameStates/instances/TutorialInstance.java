package de.zelosfan.timedstrategy.gameStates.instances;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;
import de.zelosfan.framework.GameState.Instance;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.timedstrategy.Game;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.gameStates.IngamePlanning;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 26.08.13
 * Time: 01:12
 */
public class TutorialInstance implements Instance {

    @Override
    public void onActivate() {

    }

    @Override
    public void onDeactivate() {

    }

    @Override
    public void render(Rendermanager rendermanager) {
        if (Main.map == -2) {
            rendermanager.drawIndependent(Main.textureRegionObjectMap.get("title"), 0, 0, 1, 0.8f, 0);
        } else if (Main.map == -1) {
            rendermanager.drawIndependent(Main.textureRegionObjectMap.get("starttut"), 0, 0, 1, 0.8f, 0);
        } else if (Main.map <= 4) {
            rendermanager.drawIndependent(Main.textureRegionObjectMap.get("tut" + Main.map), 0, 0, 1, 0.8f, 0);
        } else if (Main.map >= 6) {
            rendermanager.drawIndependent(Main.textureRegionObjectMap.get("end"), 0, 0, 1, 0.8f, 0);
        } else {
            Main.game = new Game(Main.map);
            Main.game.changeToPlanningPhase();
            Main.gameStateManager.setCurrentGameState(new IngamePlanning(Main.gameStateManager, true));
        }
    }

    @Override
    public void tick() {

    }

    @Override
    public void connected(Controller controller) {

    }

    @Override
    public void disconnected(Controller controller) {

    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        return false;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.W:
                return false;
            case Input.Keys.S:
                return false;
            case Input.Keys.A:
                return false;
            case Input.Keys.D:
                return false;
        }

        if (Main.map >= 0) {
            Main.game = new Game(Main.map);
            Main.game.changeToPlanningPhase();
            Main.gameStateManager.setCurrentGameState(new IngamePlanning(Main.gameStateManager, true));
        } else {
            Main.map++;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
