package de.zelosfan.timedstrategy.etc;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 05:58
 */
public enum CurrentAction {
    NOTHING, SELECTED_ENTITY, MOVE, ATTACK, TO_SELECT_ENTITY;
}
