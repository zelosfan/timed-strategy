package de.zelosfan.timedstrategy.etc;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import de.zelosfan.framework.Effect.Effect;
import de.zelosfan.framework.Effect.Modifieable;
import de.zelosfan.framework.Interpolation.TweenHandler;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.world.Building;
import de.zelosfan.timedstrategy.world.Tile;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 22:46
 */
public class RisingCoin extends Effect {
    TweenHandler handler;
    Building building;

    public RisingCoin(Building _building) {
        super(null);
        building = _building;
        handler = new TweenHandler();
        handler.add("Y", 0, 50, 60, Interpolation.linear);
        handler.add("a", 1, 0, 60, Interpolation.fade);
    }

    @Override
    public void tick() {
        super.tick();
        handler.tick();

        if (tickCount > 80) {
            finished = true;
        }

    }

    @Override
    public void render(Rendermanager rendermanager) {
        super.render(rendermanager);
        if (finished) return;
        rendermanager.spriteBatch.setColor(1, 1, 1, handler.get("a"));
        rendermanager.spriteBatch.draw(Main.textureRegionObjectMap.get("10s"), Tile.SIZE * building.posX, Tile.SIZE * building.posY + handler.get("Y"), Tile.SIZE, Tile.SIZE);
        rendermanager.spriteBatch.setColor(Color.WHITE);
    }
}
