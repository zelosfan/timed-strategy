package de.zelosfan.timedstrategy.world;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.timedstrategy.Main;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 04:18
 */
public class Tiletyp {

    public final String texture;
    public final TextureRegion textureRegion;
    public final int waycost;
    public final int cover;

    public Tiletyp(String _texture, int _waycost, int _cover) {
        texture = _texture;
        waycost = _waycost;
        cover = _cover;
        textureRegion = Main.textureRegionObjectMap.get(_texture);
    }

}
