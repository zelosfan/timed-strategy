package de.zelosfan.timedstrategy.world;


import com.badlogic.gdx.Application;
import com.badlogic.gdx.graphics.Color;
import de.zelosfan.framework.Logging.LogManager;
import de.zelosfan.framework.Util.MathUtil;
import de.zelosfan.timedstrategy.Game;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.Repository;
import de.zelosfan.timedstrategy.ai.Ai;
import de.zelosfan.timedstrategy.entity.Entity;
import de.zelosfan.timedstrategy.team.Team;
import de.zelosfan.timedstrategy.world.buildingtypes.BuildingTyp;

import java.util.ArrayList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 04:06
 */
public class World {

    public final int WORLD_WIDTH;
    public final int WORLD_HEIGHT;
    public boolean FOG_OF_WAR;
    public Tile[][] tiles;
    public ArrayList<Entity> entities = new ArrayList<>();
    public World(Integer[][] map) {
        WORLD_WIDTH = map.length;
        WORLD_HEIGHT = map[0].length;

        tiles = new Tile[WORLD_WIDTH][WORLD_HEIGHT];

        for (int i = 0; i < map.length; i++) {
            for (int l = 0; l < map[i].length; l++) {

                switch (map[i][map[i].length - l - 1]) {

                    case 483740927:
                        tiles[i][l] = new Tile(i, l, Repository.tiletypObjectMap.get("grass"));
                        break;

                    case 1179010815:
                        tiles[i][l] = new Tile(i, l, Repository.tiletypObjectMap.get("hill"));
                        break;

                    case 838880255:
                        tiles[i][l] = new Tile(i, l, Repository.tiletypObjectMap.get("stone"));
                        break;

                    case 1614353407:
                        tiles[i][l] = new Tile(i, l, Repository.tiletypObjectMap.get("dirt"));
                        break;

                    case -1415577345:
                        tiles[i][l] = new Building(i, l, (BuildingTyp) Repository.tiletypObjectMap.get("income"), null);
                        break;

                    case -388431617:
                        tiles[i][l] = new Building(i, l, (BuildingTyp) Repository.tiletypObjectMap.get("income"), Game.blueTeam);
                        break;

                    case 2104819967:
                        tiles[i][l] = new Building(i, l, (BuildingTyp) Repository.tiletypObjectMap.get("income"), Game.redTeam);
                        break;

                    case -944596481:
                        tiles[i][l] = new Building(i, l, (BuildingTyp) Repository.tiletypObjectMap.get("factory"), null);
                        break;

                    case -2123144193:
                        tiles[i][l] = new Building(i, l, (BuildingTyp) Repository.tiletypObjectMap.get("factory"), Game.redTeam);
                        break;

                    case -85933825:
                        tiles[i][l] = new Building(i, l, (BuildingTyp) Repository.tiletypObjectMap.get("factory"), Game.blueTeam);
                        break;

                    case -16776961:
                        tiles[i][l] = new Building(i, l, (BuildingTyp) Repository.tiletypObjectMap.get("hq"), Game.redTeam);
                        Game.redTeam.hq = (Building) tiles[i][l];
                        break;

                    case 65535:
                        tiles[i][l] = new Building(i, l, (BuildingTyp) Repository.tiletypObjectMap.get("hq"), Game.blueTeam);
                        Game.blueTeam.hq = (Building) tiles[i][l];
                        break;

                    default:
                        LogManager.log(World.class, Application.LOG_INFO, "(create) unused color " + map[i][map[i].length - l - 1]);
                        tiles[i][l] = new Tile(i, l, Repository.tiletypObjectMap.get("grass"));
                        break;
                }

            }

        }

        FOG_OF_WAR = true;
        switch (Main.map) {
            case 0:
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.blueTeam, tiles[8][2]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.blueTeam, tiles[7][3]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, tiles[2][2]));
                FOG_OF_WAR = false;
                break;
            case 1:
                entities.add(new Entity(Repository.entitytypObjectMap.get("infantry"), Game.blueTeam, tiles[10][3]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.blueTeam, tiles[12][3]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, tiles[1][4]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("infantry"), Game.redTeam, tiles[3][4]));
                FOG_OF_WAR = false;
                break;

            case 2:
                entities.add(new Entity(Repository.entitytypObjectMap.get("infantry"), Game.blueTeam, tiles[10][3]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("rocketInfantry"), Game.blueTeam, tiles[12][4]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.blueTeam, tiles[12][3]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("rocketInfantry"), Game.blueTeam, tiles[8][1]));

                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, tiles[3][9]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, tiles[1][7]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("infantry"), Game.redTeam, tiles[3][8]));
                FOG_OF_WAR = false;
                break;

            case 3:
                entities.add(new Entity(Repository.entitytypObjectMap.get("infantry"), Game.blueTeam, tiles[13][3]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("rocketInfantry"), Game.blueTeam, tiles[12][8]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.blueTeam, tiles[9][7]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("scout"), Game.blueTeam, tiles[11][7]));

                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, tiles[6][6]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, tiles[4][4]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("infantry"), Game.redTeam, tiles[2][7]));
                break;

            case 4:
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.blueTeam, tiles[17][5]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("infantry"), Game.blueTeam, tiles[18][5]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("rocketInfantry"), Game.blueTeam, tiles[16][5]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("scout"), Game.blueTeam, tiles[14][5]));


                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, tiles[3][13]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, tiles[2][13]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, Game.redTeam.hq));
                break;

            case 5:
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.blueTeam, Game.blueTeam.hq));
                entities.add(new Entity(Repository.entitytypObjectMap.get("infantry"), Game.blueTeam, tiles[18][5]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("rocketInfantry"), Game.blueTeam, tiles[16][5]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("scout"), Game.blueTeam, tiles[14][5]));


                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, tiles[13][17]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("scout"), Game.redTeam, tiles[14][17]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("infantry"), Game.redTeam, tiles[15][15]));
                entities.add(new Entity(Repository.entitytypObjectMap.get("test"), Game.redTeam, Game.redTeam.hq));
                break;
        }

        entities.size();
    }

    public int getIntelligentWaycost(Tile tile, Entity entity) {
        float waycost = 0;

        //Add fight cost
        for (int i = -1; i <= 1; i++) {
            for (int l = -1; l <= 1; l++) {
                if (Math.abs(i) != Math.abs(l)) {
                    if (entity.tile.posX + i > 0 && entity.tile.posX + i < WORLD_WIDTH && entity.tile.posY + l > 0 && entity.tile.posY + l < WORLD_HEIGHT) {
                        if (tiles[entity.tile.posX + i][entity.tile.posY + l].entity != null) {
                            if (!tiles[entity.tile.posX + i][entity.tile.posY + l].entity.team.equals(entity.team)) {
                                waycost += Main.game.calculateFightCost(entity, tile,tiles[entity.tile.posX + i][entity.tile.posY + l].entity);
                            }
                        }
                    }
                }
            }
        }
        //#####################


        waycost +=  (3 - tile.tiletyp.cover) *  Ai.COVER_WEALTH;
        waycost += tile.tiletyp.waycost * Ai.WAYCOST_COST;

        waycost += Ai.MAX_BUILDING_WEALTH;

        if (tile.tiletyp instanceof BuildingTyp) {
            Building building = (Building) tile;
            BuildingTyp buildingTyp = (BuildingTyp) tile.tiletyp;

            float multiplier = 1;
            if (building.team == null) {
                multiplier = 1.5f;
            } else if (building.team.equals(Game.blueTeam)) {
                multiplier = 2f;
            }

            waycost -= buildingTyp.wealth * multiplier * Ai.BUILDING_WEALTH_MULTIPLIER;
        }

        return (int) waycost;
    }

}
