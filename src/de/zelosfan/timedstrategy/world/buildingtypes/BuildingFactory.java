package de.zelosfan.timedstrategy.world.buildingtypes;

import com.badlogic.gdx.graphics.Color;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.Repository;
import de.zelosfan.timedstrategy.entity.Entity;
import de.zelosfan.timedstrategy.entity.Entitytyp;
import de.zelosfan.timedstrategy.etc.CurrentAction;
import de.zelosfan.timedstrategy.etc.RisingCoin;
import de.zelosfan.timedstrategy.gameStates.instances.MapDisplay;
import de.zelosfan.timedstrategy.world.Building;
import de.zelosfan.timedstrategy.world.Tile;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 21:31
 */
public class BuildingFactory extends BuildingTyp{

    public BuildingFactory(String texture, int waycost, int cover) {
        super(texture, waycost, cover, 3);
    }

    @Override
    public void nextRound(Building building) {
        if (building.obj != null) {
            Main.game.world.entities.add(new Entity( ((Entitytyp) building.obj), building.team, building));
            building.obj = null;
        }
    }

    @Override
    public void onClick(Building building) {
        super.onClick(building);
        if (building.team == null) return;
        if (!building.team.equals(Main.game.blueTeam)) return;
        if (building.obj != null) return;
        if (Main.game.currentAction == CurrentAction.SELECTED_ENTITY) return;

        Main.game.currentAction = CurrentAction.TO_SELECT_ENTITY;
        Main.game.current0 = building;
        Main.game.showMessage = "Select a Unittyp to produce!";
    }

    public void render(Rendermanager rendermanager, Building building) {
        if (building.team == null || building.obj == null) return;

        rendermanager.spriteBatch.setColor(Repository.used);
        rendermanager.spriteBatch.draw(((Entitytyp) building.obj).textureRegion, Tile.SIZE * building.posX, Tile.SIZE * building.posY , Tile.SIZE, Tile.SIZE);
        rendermanager.spriteBatch.setColor(Color.WHITE);

        rendermanager.spriteBatch.draw(Main.textureRegionObjectMap.get("check"), Tile.SIZE * building.posX + Tile.SIZE * 0.6f, Tile.SIZE * building.posY + Tile.SIZE * 0.6f, Tile.SIZE * 0.45f, Tile.SIZE * 0.45f);
    }
}
