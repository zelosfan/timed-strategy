package de.zelosfan.timedstrategy;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.framework.Audio.AudioManager;
import de.zelosfan.framework.GameState.GameStateManager;
import de.zelosfan.framework.IO.ResourceLoader;
import de.zelosfan.framework.Intelligence.PlatformInformation;
import de.zelosfan.framework.Logging.LogManager;
import de.zelosfan.framework.Rendering.CameraManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.framework.Rendering.Rendermode;
import de.zelosfan.framework.Timing.GametimeManager;
import de.zelosfan.timedstrategy.gameStates.Tutorial;
import de.zelosfan.timedstrategy.gameStates.instances.MapDisplay;
import de.zelosfan.timedstrategy.gameStates.instances.TutorialInstance;


/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 03:36
 */
public class Main extends Game{

    //I only got 48h ^_^
    public static ObjectMap<String, TextureRegion> textureRegionObjectMap = new ObjectMap<>();
    public static ObjectMap<String, ShaderProgram> shaderProgramObjectMap = new ObjectMap<>();
    public static ObjectMap<String, Sound> sfxMap = new ObjectMap<>();

    public static ObjectMap<String, ObjectMap<Integer, BitmapFont>> fontMap = new ObjectMap<>();

    public static ObjectMap<String, Integer[][]> mapObjectMap = new ObjectMap<>();

    public static de.zelosfan.timedstrategy.Game game;
    public static GameStateManager gameStateManager;
    public static CameraManager cameraManager;
    public static Rendermanager rendermanager;
    public static AudioManager audioManager;
    //######################

    public static int map = -2;


    //CONSTATNS
    public static final int TPS = 60;
    public static final String MUSICPATH = "music/";
    public static final String SOUNDPATH = "sfx/";

    public static final float SCROLLSPEED = 7f;
    //#############

    public static boolean FULLSCREEN = false;
    public static boolean SHOW_HELP = true;
    GametimeManager gametimeManager;



//    IOManager ioManager;

    ResourceLoader resourceLoader;


    @Override
    public void create() {
        LogManager.DEFAULT_LOGLEVEL = Application.LOG_DEBUG;
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        resourceLoader = new ResourceLoader();
        resourceLoader.loadImagePackIntoTextureRegions("tiles.pack", textureRegionObjectMap);
        resourceLoader.loadImagePackIntoTextureRegions("pics.pack", textureRegionObjectMap);
        resourceLoader.loadImagePackIntoTextureRegions("ui.pack", textureRegionObjectMap);
        resourceLoader.loadImagePackIntoTextureRegions("entities.pack", textureRegionObjectMap);
        ResourceLoader.loadShaders("shader/", shaderProgramObjectMap);
        resourceLoader.loadFontSizes("font.pack", fontMap);
        resourceLoader.loadImagePackIntoPixelInteger("maps.pack", mapObjectMap);
        resourceLoader.loadDirectoryFiles(SOUNDPATH, sfxMap, Sound.class);
        resourceLoader.finishLoading();

        gametimeManager = new GametimeManager(TPS);
        gameStateManager = new GameStateManager();

        rendermanager = new Rendermanager(Rendermode.WORLD, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), shaderProgramObjectMap);
        cameraManager = new CameraManager(rendermanager.camera);

        audioManager = new AudioManager(0.5f, 0.5f, false, false, sfxMap);
        audioManager.getMusicFiles(MUSICPATH);
        audioManager.playNextMusicTrack();

        Main.gameStateManager.instanceHashMap.put("Tutorial", new TutorialInstance());
        Main.gameStateManager.setCurrentGameState(new Tutorial(Main.gameStateManager, true));

//        PlatformInformation.setFullscreen(true);
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void render() {
        gametimeManager.updateTime(Gdx.graphics.getRawDeltaTime());

//        if (game == null) {
//            game = new de.zelosfan.timedstrategy.Game(map);
//            game.changeToPlanningPhase();
//        }


        while (gametimeManager.tick()) {
            tick();
        }

        audioManager.tick();
        rendermanager.render(gameStateManager);
    }

    public void tick() {
        gameStateManager.tick();

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
//        rendermanager.resize(width, height);
    }
}
