package de.zelosfan.timedstrategy.etc;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import de.zelosfan.framework.Effect.Effect;
import de.zelosfan.framework.Effect.SimpleParticle;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.framework.Util.RandomUtil;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.entity.Entity;
import de.zelosfan.timedstrategy.world.Tile;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 22:46
 */
public class Reste extends Effect {
    Tile tile;
    SimpleParticle[] simpleParticles;


    public Reste(Entity _entity) {
        super(null);
        tile =_entity.tile;

        simpleParticles = new SimpleParticle[RandomUtil.getNanoRandom().nextInt(4) + 4];

//        positions.add("X", 0, 0, 0, Interpolation.circle);
//        positions.add("Y", 0, 0, 0, Interpolation.circle);
//        positions.add("sizeX", 0, 0, 0, Interpolation.circle);
//        positions.add("sizeY", 0, 0, 0, Interpolation.circle);
//        positions.add("rotation", 0, 0, 0, Interpolation.circle);
//        positions.add("alpha", 0, 0, 0, Interpolation.circle);

        for (int i = 0; i < simpleParticles.length; i++) {
            simpleParticles[i] = new SimpleParticle(Main.textureRegionObjectMap.get("particle1"), 600, _entity.offsetX + RandomUtil.getNanoRandom().nextInt(30) - 15, _entity.offsetY + RandomUtil.getNanoRandom().nextInt(10) - 5, 20, 20, 0, 1);

            simpleParticles[i].positions.add("X", 0, _entity.offsetX + RandomUtil.getNanoRandom().nextInt(80) - 40, 60, Interpolation.circleIn);
//            simpleParticles[i].positions.add("alpha", 1, 1, 40, Interpolation.linear).add(0, RandomUtil.getNanoRandom().nextInt(20), Interpolation.linear);
            simpleParticles[i].positions.add("Y", 0, _entity.offsetY + RandomUtil.getNanoRandom().nextInt(10) + 20, 40, Interpolation.circleIn).add(RandomUtil.getNanoRandom().nextInt(60) * -1, 80, Interpolation.bounceOut);
//            simpleParticles[i].positions.setDestination("Y", RandomUtil.getNanoRandom().nextInt(20));

        }
    }

    @Override
    public void tick() {
        super.tick();

        for (SimpleParticle simpleParticle: simpleParticles) {
            simpleParticle.tick();
        }

    }

    @Override
    public void render(Rendermanager rendermanager) {
        super.render(rendermanager);

        for (SimpleParticle simpleParticle: simpleParticles) {
            if (simpleParticle.alive) {
                rendermanager.spriteBatch.setColor(1, 1 , 1, simpleParticle.getAlpha());
                rendermanager.spriteBatch.draw(simpleParticle.getTexture(), Tile.SIZE * tile.posX + simpleParticle.getX() + Tile.SIZE * 0.5f, Tile.SIZE * tile.posY  + simpleParticle.getY() + Tile.SIZE * 0.5f, simpleParticle.getSizeX(), simpleParticle.getSizeY());
            }
        }
        rendermanager.spriteBatch.setColor(Color.WHITE);
    }
}
