package de.zelosfan.timedstrategy.entity;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.Repository;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 04:56
 */
public class Entitytyp {
    public final int speed;
    public final int sight;
    public final int cost;
    public final String texture;
    public final TextureRegion textureRegion;
    public final ObjectMap<Entitytyp, Float> attackStats = new ObjectMap<>();

    public Entitytyp(String _texture, int _cost, int _speed, int _sight) {
        texture = _texture;
        textureRegion = Main.textureRegionObjectMap.get(_texture);
        speed = _speed;
        sight = _sight;
        cost = _cost;
    }

    public Entitytyp add(String entitytyp, float power) {
        attackStats.put(Repository.entitytypObjectMap.get(entitytyp), power);
        return this;
    }
}
