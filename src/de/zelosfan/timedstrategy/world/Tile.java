package de.zelosfan.timedstrategy.world;

import com.badlogic.gdx.math.Vector2;
import de.zelosfan.framework.Algorithms.Pathable;
import de.zelosfan.timedstrategy.entity.Entity;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 04:17
 */
public class Tile implements Pathable{

    public static final int SIZE = 60;

    public Tiletyp tiletyp;
    public final int posX, posY;
    public Entity entity;
    public boolean locked = false;
//    public Entity entity;

    public Tile(int _posX, int _posY, Tiletyp _tiletyp) {
        posX = _posX;
        posY = _posY;
        tiletyp = _tiletyp;
    }

    @Override
    public Vector2 getPositionV2() {
        return new Vector2(posX, posY);
    }

    @Override
    public int getWaycost() {
        return tiletyp.waycost;
    }

    @Override
    public boolean isWalkable(boolean isOrigin) {
        return (isOrigin || !locked) && tiletyp.waycost < 99;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if ((obj instanceof  Pathable)) return ((Pathable) obj).getPositionV2().x == posX && ((Pathable) obj).getPositionV2().y == posY;
        return ((Tile) obj).posX == posX && ((Tile) obj).posY == posY;
    }
}
