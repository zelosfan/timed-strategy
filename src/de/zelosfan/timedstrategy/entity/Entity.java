package de.zelosfan.timedstrategy.entity;

import de.zelosfan.framework.Algorithms.Pathable;
import de.zelosfan.timedstrategy.Repository;
import de.zelosfan.timedstrategy.team.Team;
import de.zelosfan.timedstrategy.world.Tile;

import java.util.LinkedList;
import java.util.Queue;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 04:56
 */
public class Entity {

    public final Team team;
    public final Entitytyp entitytyp;
    public Tile tile;
    public LinkedList<Pathable> waypoints;
    public Tile target = null;

    public int waycost = 0;

    public float offsetX = 0;
    public float offsetY = 0;

    public int health;

    public Entity(Entitytyp _entitytyp, Team _team, Tile _tile) {
        health = 100;
        team = _team;
        entitytyp = _entitytyp;

        tile = _tile;
        tile.locked = true;
        _tile.entity = this;
    }

    public void damage(int damage) {
        health -= (damage) * Repository.coverBonus.get(tile.tiletyp.cover);
    }

}
