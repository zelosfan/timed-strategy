package de.zelosfan.timedstrategy.gameStates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import de.zelosfan.framework.GameState.GameState;
import de.zelosfan.framework.GameState.GameStateManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.framework.Timing.GametimeManager;
import de.zelosfan.timedstrategy.Game;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.entity.Entity;
import de.zelosfan.timedstrategy.world.Building;
import de.zelosfan.timedstrategy.world.Tile;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 03:57
 */
public class IngameAttack extends GameState{
    public int tickC = 0;

    public IngameAttack(GameStateManager gameStateManager, boolean _inputPreProcessor) {
        super(gameStateManager, _inputPreProcessor);
        tickC = GametimeManager.realtimeToGametime(5);
    }

    @Override
    public void render(Rendermanager rendermanager) {
        super.render(rendermanager);

        gameStateManager.instanceHashMap.get("MapDisplay").render(rendermanager);
        gameStateManager.instanceHashMap.get("UserInterface").render(rendermanager);
    }

    @Override
    protected InputMultiplexer buildInputMultiplexer() {
        return new InputMultiplexer(gameStateManager.instanceHashMap.get("MapDisplay"));
    }

    @Override
    public void onActivate() {
        gameStateManager.instanceHashMap.get("UserInterface").onActivate();
        gameStateManager.instanceHashMap.get("MapDisplay").onActivate();
//        Main.audioManager.loopSound(Main.sfxMap.get("drive0.wav"));

        for (Entity entity: Main.game.world.entities) {
            entity.waycost = entity.entitytyp.speed;
            Main.game.attackAround(entity);
        }
        Main.game.checkEntities();

    }

    @Override
    public void onDeactivate() {
        gameStateManager.instanceHashMap.get("UserInterface").onDeactivate();
        gameStateManager.instanceHashMap.get("MapDisplay").onDeactivate();
    }

    @Override
    public void tick() {

        Main.game.tick();
        tickC--;
        gameStateManager.instanceHashMap.get("MapDisplay").tick();
        gameStateManager.instanceHashMap.get("UserInterface").tick();
        Main.game.timeLeft = tickC;
        if (tickC <= 0) {
            Main.game.changeToPlanningPhase();
        }

        boolean finished = true;
        for (Entity entity: Main.game.world.entities) {

            if (entity.waypoints != null) {
                if (entity.waypoints.peek() != null) {
                    if (entity.offsetX > Tile.SIZE / 2) {
                        entity.tile.entity = null;
                        entity.tile.locked = false;
                        entity.tile = Main.game.world.tiles[entity.tile.posX + 1][entity.tile.posY];
                        entity.tile.entity = entity;
                        entity.offsetX = -Tile.SIZE / 2 + 1;
                        Main.game.attackAround(entity);
                        Main.game.updateFogOfWar();
                        finished = false;
                        continue;
                    } else if (entity.offsetX < -Tile.SIZE / 2) {
                        entity.tile.entity = null;
                        entity.tile.locked = false;
                        entity.tile = Main.game.world.tiles[entity.tile.posX - 1][entity.tile.posY];
                        entity.tile.entity = entity;
                        entity.offsetX = Tile.SIZE / 2 - 1;
                        Main.game.attackAround(entity);
                        Main.game.updateFogOfWar();
                        finished = false;
                        continue;
                    } else  if (entity.offsetY > Tile.SIZE / 2) {
                        entity.tile.entity = null;
                        entity.tile.locked = false;
                        entity.tile = Main.game.world.tiles[entity.tile.posX][entity.tile.posY + 1];
                        entity.tile.entity = entity;
                        entity.offsetY = -Tile.SIZE / 2 + 1;
                        Main.game.attackAround(entity);
                        Main.game.updateFogOfWar();
                        finished = false;
                        continue;
                    } else if (entity.offsetY < -Tile.SIZE / 2) {
                        entity.tile.entity = null;
                        entity.tile.locked = false;
                        entity.tile = Main.game.world.tiles[entity.tile.posX][entity.tile.posY - 1];
                        entity.tile.entity = entity;
                        entity.offsetY = Tile.SIZE / 2 - 1;
                        Main.game.attackAround(entity);
                        Main.game.updateFogOfWar();
                        finished = false;
                        continue;
                    }

                    int speed = 2;

                    if (!entity.tile.equals(entity.waypoints.peek())) {
                        finished = false;
                        int dirX =  (entity.tile.posX - (int) entity.waypoints.peek().getPositionV2().x);
                        int dirY =  (entity.tile.posY - (int) entity.waypoints.peek().getPositionV2().y);

                        if (dirX != 0) {
                            entity.offsetX -= dirX * speed;
                        } else if (dirY != 0) {
                            entity.offsetY -= dirY * speed;
                        }

                    } else if (entity.offsetX != 0 || entity.offsetY != 0){
                        finished = false;
                        if (entity.offsetX != 0) {
                            if (Math.abs(entity.offsetX) < speed) {
                                entity.offsetX = 0;
                            } else {
                                entity.offsetX -= entity.offsetX / Math.abs(entity.offsetX) * speed;
                            }
                        }
                        if (entity.offsetY != 0) {
                            if (Math.abs(entity.offsetY) < speed) {
                                entity.offsetY = 0;
                            } else {
                                entity.offsetY -= entity.offsetY / Math.abs(entity.offsetY) * speed;
                            }
                        }
                    }

                    if (entity.tile.equals(entity.waypoints.peek()) && entity.offsetX == 0 && entity.offsetY == 0) {
                        if (entity.waypoints.peek() != null) {
                            if ((((Tile) entity.waypoints.peek()).locked) && !entity.equals(((Tile) entity.waypoints.peek()).entity)) {
                            } else {

                                if (entity.waypoints.size() > 1) {
                                    if (!((Tile) entity.waypoints.get(1)).locked && (((Tile) entity.waypoints.get(1)).tiletyp.waycost <= entity.waycost || entity.team.equals(Game.blueTeam))) {
                                        finished = false;
                                        if (entity.tile instanceof Building) {
                                            Building building = (Building) entity.tile;

                                            if (building.team != null) {
                                                if (building.team.equals(entity.team)) {
                                                    entity.waypoints.poll();

                                                    if (entity.waypoints.size() > 0) {
                                                        entity.waycost -= ((Tile) entity.waypoints.peek()).tiletyp.waycost;
                                                    }
                                                }
                                            }
                                        } else {

                                            entity.waypoints.poll();
                                            if (entity.waypoints.size() > 0) {
                                                entity.waycost -= ((Tile) entity.waypoints.peek()).tiletyp.waycost;
                                            }
                                        }
                                    } else {
                                        ((Tile) entity.waypoints.peek()).locked = false;
//
                                        if (((Tile) entity.waypoints.get(1)).entity != null) {
                                            if (!((Tile) entity.waypoints.get(1)).entity.team.equals(entity.team)) {
//                                                Main.game.fightEntities(entity, ((Tile) entity.waypoints.get(1)).entity);
                                            }
                                        }
                                    }
                                } else {
                                    entity.waypoints.poll();
                                    if (entity.waypoints.size() > 0) {
                                        entity.waycost -= ((Tile) entity.waypoints.peek()).tiletyp.waycost;
                                    }
                                }

                                if (entity.waypoints.peek() != null) {
                                    ((Tile) entity.waypoints.peek()).locked = true;
                                }
                            }
                        }

                    }

                }

            }
        }



        Main.game.checkEntities();

        if (finished) {
            Main.game.changeToPlanningPhase();
        }

    }
}
