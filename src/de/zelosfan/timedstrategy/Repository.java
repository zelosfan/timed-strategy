package de.zelosfan.timedstrategy;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.timedstrategy.entity.Entitytyp;
import de.zelosfan.timedstrategy.world.Tiletyp;
import de.zelosfan.timedstrategy.world.buildingtypes.BuildingFactory;
import de.zelosfan.timedstrategy.world.buildingtypes.BuildingHeadquarter;
import de.zelosfan.timedstrategy.world.buildingtypes.BuildingIncome;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 04:21
 */
public class Repository {

    public static ObjectMap<String, Tiletyp> tiletypObjectMap = new ObjectMap<>();
    public static ObjectMap<String, Entitytyp> entitytypObjectMap = new ObjectMap<>();
    public static ObjectMap<Integer, Float> coverBonus = new ObjectMap<>();

    public static Color used = new Color(0.2f, 0.2f, 0.2f,0.8f);
    public static Color grey = new Color(0.2f, 0.2f, 0.2f, 1f);
    public static Color red = new Color(1f, 0f, 0f, 0.8f);
//    public static Color commmanded = new Color(0.4f,0.4f,0.4f,0.8f);
    static {
        //Create all Tiletyps and stuff
        coverBonus.put(0, 1f);
        coverBonus.put(1, 0.8f);
        coverBonus.put(2, 0.6f);
        coverBonus.put(3, 0.5f);

        tiletypObjectMap.put("grass", new Tiletyp("grass", 6, 1));
        tiletypObjectMap.put("stone", new Tiletyp("stone", 4, 0));
        tiletypObjectMap.put("dirt", new Tiletyp("dirt", 8, 0));
        tiletypObjectMap.put("hill", new Tiletyp("hill", 99, 0));
        tiletypObjectMap.put("income", new BuildingIncome("building", 6, 3, 3, 1));
        tiletypObjectMap.put("factory", new BuildingFactory("factory", 6, 2));
        tiletypObjectMap.put("hq", new BuildingHeadquarter("hq", 8, 3));


        entitytypObjectMap.put("test", new Entitytyp("test", 4, 20, 5));
        entitytypObjectMap.put("rocketInfantry", new Entitytyp("rocketInfantry", 2, 12, 3));
        entitytypObjectMap.put("infantry", new Entitytyp("infantry", 1, 12, 3));
        entitytypObjectMap.put("scout", new Entitytyp("scout", 3, 32, 6));

        entitytypObjectMap.get("test").add("test", 40).add("rocketInfantry", 80).add("infantry", 80).add("scout", 100);
        entitytypObjectMap.get("rocketInfantry").add("test", 60).add("rocketInfantry", 20).add("infantry", 20).add("scout", 80);
        entitytypObjectMap.get("infantry").add("test", 10).add("rocketInfantry", 70).add("infantry", 70).add("scout", 10);
        entitytypObjectMap.get("scout").add("test", 10).add("rocketInfantry", 70).add("infantry", 70).add("scout", 10);
    }

}
