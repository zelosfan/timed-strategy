package de.zelosfan.timedstrategy.team;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import de.zelosfan.timedstrategy.Game;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.world.Building;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 05:01
 */
public class Team {
    public final Color color;
    public int money = 2;
    public final boolean flip;
    public Building hq = null;

    public Team(Color _color, boolean _flip) {
        color = _color;
        flip = _flip;
    }

    @Override
    public boolean equals(Object obj) {
        return color.equals(((Team) obj).color);
    }

    public void moneyadd(int amount) {
        money += amount;
        if (this.equals(Game.blueTeam)) {
            Main.audioManager.playSound("coin1.wav");
        }
    }

    public boolean moneydeduct(int amount) {
        if (money < amount) return false;
        money -= amount;
        if (this.equals(Game.blueTeam)) {
            Main.audioManager.playSound("coin0.wav");
        }
        return true;
    }
}
