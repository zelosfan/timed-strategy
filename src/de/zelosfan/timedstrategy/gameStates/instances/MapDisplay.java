package de.zelosfan.timedstrategy.gameStates.instances;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import de.zelosfan.framework.Algorithms.Pathable;
import de.zelosfan.framework.GameState.Instance;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.framework.Timing.GametimeManager;
import de.zelosfan.timedstrategy.Game;
import de.zelosfan.timedstrategy.Main;
import de.zelosfan.timedstrategy.Repository;
import de.zelosfan.timedstrategy.entity.Entity;
import de.zelosfan.timedstrategy.etc.CurrentAction;
import de.zelosfan.timedstrategy.world.Building;
import de.zelosfan.timedstrategy.world.Tile;
import de.zelosfan.timedstrategy.world.buildingtypes.BuildingTyp;

import java.awt.*;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.08.13
 * Time: 03:57
 */
public class MapDisplay implements Instance{
    public Vector2 cameraMove = new Vector2();


    @Override
    public void onActivate() {

    }

    @Override
    public void onDeactivate() {

    }

    public static void drawIndependentText(Rendermanager rendermanager, BitmapFont font, String shader, String text, float x, float y, Color color) {
        rendermanager.spriteBatch.setShader(Main.shaderProgramObjectMap.get(shader));
        font.setColor(color);
        font.draw(rendermanager.spriteBatch, text, x, y);
        rendermanager.clearShader();
        rendermanager.spriteBatch.setShader(null);
    }

    @Override
    public void render(Rendermanager rendermanager) {
        for (int x = 0; x < Main.game.world.WORLD_WIDTH; x++) {
            for (int y = Main.game.world.WORLD_HEIGHT - 1; y >= 0; y--) {

                if (Main.game.visible != null) {
                    if (!Main.game.visible.contains(Main.game.world.tiles[x][y]) && Main.game.world.FOG_OF_WAR) {
                        if (Main.game.world.tiles[x][y] instanceof Building) {
                            if (((Building) Main.game.world.tiles[x][y]).team != null) {
                                if (!((Building) Main.game.world.tiles[x][y]).team.equals(Main.game.blueTeam)) {
                                    rendermanager.spriteBatch.setColor(Repository.grey);
                                }
                            } else {
                                rendermanager.spriteBatch.setColor(Repository.grey);
                            }
                        } else {
                            rendermanager.spriteBatch.setColor(Repository.grey);
                        }
                    }
                }

                switch (Main.game.currentAction) {
                    case SELECTED_ENTITY:

                        if (Main.game.avaible != null) {
                            if (Main.game.avaible.contains(Main.game.world.tiles[x][y])) {
                                rendermanager.spriteBatch.setColor(Color.BLUE);
                            }
                        }
                        if (Main.game.highlighted != null) {
                            if (Main.game.highlighted.contains(Main.game.world.tiles[x][y])) {
                                rendermanager.spriteBatch.setColor(Repository.red);
                            }
                        }


                        break;
                }

                if (Main.game.world.tiles[x][y] instanceof Building) {
                    Building building = (Building) Main.game.world.tiles[x][y];
                    rendermanager.spriteBatch.draw(Main.textureRegionObjectMap.get("stone"), Tile.SIZE * x, Tile.SIZE * y, Tile.SIZE, Tile.SIZE);

                    if (building.team != null) {
                        if (building.team.equals(Main.game.blueTeam) || !Main.game.world.FOG_OF_WAR || Main.game.visible.contains(Main.game.world.tiles[x][y])|| building.tiletyp.equals(Repository.tiletypObjectMap.get("hq"))) {
                            rendermanager.spriteBatch.setColor(building.team.color);
                        }
                    } else {
                        rendermanager.spriteBatch.setColor(Repository.grey);
                    }

                }
//                if (Main.game.world.tiles[x][y].locked) {
//                    rendermanager.spriteBatch.setColor(Color.PINK);
//                }
                    rendermanager.spriteBatch.draw(Main.game.world.tiles[x][y].tiletyp.textureRegion, Tile.SIZE * x, Tile.SIZE * y, Tile.SIZE, Tile.SIZE);
                rendermanager.spriteBatch.setColor(Color.WHITE);

                if (Main.game.world.tiles[x][y] instanceof Building) {
                    ((BuildingTyp) Main.game.world.tiles[x][y].tiletyp).render(rendermanager, (Building) Main.game.world.tiles[x][y]);
                }
            }
        }

        for (Entity entity: Main.game.world.entities) {
                if (entity != null) {
                    if (Main.game.visible.contains(entity.tile) || entity.team.equals(Main.game.blueTeam) || !Main.game.world.FOG_OF_WAR) {
                        if (entity.team.flip) {
                           entity.entitytyp.textureRegion.flip(true, false);
                        }


                        if (entity.waypoints != null) {
                            if (!entity.waypoints.isEmpty() && entity.team.equals(Game.blueTeam)) {
                               rendermanager.spriteBatch.setColor(Repository.used);
                               rendermanager.spriteBatch.draw(entity.entitytyp.textureRegion, Tile.SIZE * ((Tile) entity.waypoints.getLast()).posX, Tile.SIZE * ((Tile) entity.waypoints.getLast()).posY, Tile.SIZE, Tile.SIZE);
                            }
                        }
                        rendermanager.spriteBatch.setColor(entity.team.color);

                        if (Main.game.currentAction == CurrentAction.TO_SELECT_ENTITY) {
                            if (GametimeManager.TICKCOUNT % 60 < 30) {
                                rendermanager.spriteBatch.setColor(Color.YELLOW);
                            }
                        }


                        rendermanager.spriteBatch.draw(entity.entitytyp.textureRegion, Tile.SIZE * entity.tile.posX + entity.offsetX, Tile.SIZE * entity.tile.posY + entity.offsetY, Tile.SIZE, Tile.SIZE);
                        rendermanager.spriteBatch.setColor(Color.WHITE);

                        if (Main.game.currentAction == CurrentAction.TO_SELECT_ENTITY) {
                            if (entity.entitytyp.cost <= Main.game.blueTeam.money) {
                                rendermanager.spriteBatch.setColor(Color.GREEN);
                            } else {
                                rendermanager.spriteBatch.setColor(Color.RED);
                            }
                            rendermanager.spriteBatch.draw(Main.textureRegionObjectMap.get("highlighter"), Tile.SIZE * entity.tile.posX + entity.offsetX + Tile.SIZE * 0.1f, Tile.SIZE * entity.tile.posY + entity.offsetY, Tile.SIZE * 0.8f, Tile.SIZE * 0.4f);
                            rendermanager.spriteBatch.setColor(Color.WHITE);
                            rendermanager.spriteBatch.draw(Main.textureRegionObjectMap.get("10s"), Tile.SIZE * entity.tile.posX + entity.offsetX + Tile.SIZE * 0.4f, Tile.SIZE * entity.tile.posY + entity.offsetY, Tile.SIZE * 0.4f, Tile.SIZE * 0.4f);
                            drawIndependentText(rendermanager, Main.fontMap.get("tektonpro").get(24), "font", "" + entity.entitytyp.cost, Tile.SIZE * entity.tile.posX + entity.offsetX + Tile.SIZE * 0.2f, Tile.SIZE * entity.tile.posY + entity.offsetY + Tile.SIZE * 0.5f, Color.WHITE);
                        } else if (entity.health / 10 < 10) {
                            rendermanager.spriteBatch.draw(Main.textureRegionObjectMap.get("heart"), Tile.SIZE * entity.tile.posX + entity.offsetX, Tile.SIZE * entity.tile.posY + entity.offsetY, Tile.SIZE * 0.6f, Tile.SIZE * 0.6f);
                            drawIndependentText(rendermanager, Main.fontMap.get("tektonpro").get(24), "font", "" + Math.max(entity.health / 10, 1), Tile.SIZE * entity.tile.posX + entity.offsetX + Tile.SIZE * 0.15f, Tile.SIZE * entity.tile.posY + entity.offsetY + Tile.SIZE * 0.6f, Color.WHITE);
                        }

                        if (entity.waypoints != null && entity.team.equals(Game.blueTeam)) {
                            if (!entity.waypoints.isEmpty() && Main.game.currentAction != CurrentAction.ATTACK) {
                                rendermanager.spriteBatch.draw(Main.textureRegionObjectMap.get("check"), Tile.SIZE * entity.tile.posX + entity.offsetX + Tile.SIZE * 0.6f, Tile.SIZE * entity.tile.posY + entity.offsetY + Tile.SIZE * 0.6f, Tile.SIZE * 0.45f, Tile.SIZE * 0.45f);
                            }
                        }

                        if (entity.team.flip) {
                           entity.entitytyp.textureRegion.flip(true, false);
                        }
                    }
                }
        }
        for (int x = 0; x < Main.game.world.WORLD_WIDTH; x++) {
            for (int y = Main.game.world.WORLD_HEIGHT - 1; y >= 0; y--) {
                if (Main.game.world.tiles[x][y] instanceof Building) {

                    Building building = (Building) Main.game.world.tiles[x][y];

                    Color color = Repository.grey;

                    if (building.team != null) {
                        if (building.team.equals(Main.game.blueTeam) || Main.game.visible.contains(building) || !Main.game.world.FOG_OF_WAR || building.tiletyp.equals(Repository.tiletypObjectMap.get("hq"))) {
                            color = building.team.color;
                        }
                    }

                    if (Main.game.world.tiles[x][y + 1].entity != null) {
                        rendermanager.spriteBatch.setColor(color.r, color.g, color.b, 0.6f);
                    } else {
                        rendermanager.spriteBatch.setColor(color);
                    }

                    rendermanager.spriteBatch.draw(((BuildingTyp) Main.game.world.tiles[x][y].tiletyp).textureRegion2, Tile.SIZE * x, Tile.SIZE * (y + 1), Tile.SIZE, Tile.SIZE);
                    rendermanager.spriteBatch.setColor(Color.WHITE);
                }

            }
        }

    }

    @Override
    public void tick() {
        Main.cameraManager.addMain(cameraMove.x * Main.SCROLLSPEED, cameraMove.y * Main.SCROLLSPEED);
    }

    @Override
    public void connected(Controller controller) {

    }

    @Override
    public void disconnected(Controller controller) {

    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        return false;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.W:
                cameraMove.y += 1;
                return true;
            case Input.Keys.S:
                cameraMove.y -= 1;
                return true;
            case Input.Keys.A:
                cameraMove.x -= 1;
                return true;
            case Input.Keys.D:
                cameraMove.x += 1;
                return true;
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Input.Keys.W:
                cameraMove.y -= 1;
                return true;
            case Input.Keys.S:
                cameraMove.y += 1;
                return true;
            case Input.Keys.A:
                cameraMove.x += 1;
                return true;
            case Input.Keys.D:
                cameraMove.x -= 1;
                return true;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
